//Método Fill
//---------------------------------
//Arreglos 
const array1 = [1, 2, 3, 4];
const array2 = [1, 2, 3, 4];
//-----------------------------------
// Llenar con 5 desde 1
console.log(array1.fill(5, 1));
// Esperado: [1, 5, 5, 5]
//------------------------------------
//Llenar con 6
console.log(array2.fill(6));
// Esperado: [6, 6, 6, 6]

//Método Filter
//-------------------------------------
//Arreglo
const palabras = ['spray', 'limit', 'elite', 'exuberante', 'destrucción', 'presente'];
//-------------------------------------
//Parametro
const resultado = palabras.filter(palabra => palabra.length > 6);
//-------------------------------------
console.log(resultado);
// Esperado: Arreglo ["exuberante", "destrucción", "presente"]

//Método Find
//-------------------------------------
//Arreglos
const arreglo1 = [10, 20, 35, 45, 70];
const arreglo2 = [100, 120, 150, 200, 280];
//-------------------------------------
//Encontrar
const encontrar1 = arreglo1.find(element => element < 20 );
const encontrar2 = arreglo2.find(element => element > 100);
//-------------------------------------
console.log("El elemento del arreglo es el : " + encontrar1);
console.log("El elemento del arreglo es el : " + encontrar2);
//Esperado: 45 Esperado: 120

//Método Include
//--------------------------------------
//Arreglo
const Mascotas = ['Gato', 'Perro', 'Conejo'];
//--------------------------------------
//Devuelve
console.log("Disponibilidad de la mascota seleccionada : " + Mascotas.includes('Conejo'));
// Esperado: true
//--------------------------------------
//Devuelve
console.log("Disponibilidad de la mascota seleccionada : " + Mascotas.includes('Hamster'));
// Esperado: false