//Metodo Get Of
var proto = {};
var obj= Object.create(proto);
Object.getPrototypeOf(obj) === proto;
console.log(obj);
//---------------------------------------------------------
//Metodo Set Of
var dict = Object.setPrototypeOf({}, null);
console.log(dict);
//---------------------------------------------------------
//Metodo Prevent Extensions
var obj1 = {
    Raza : "Dalmata",
    Edad : 10,
    Nombre : "Max"
};
var obj2 = Object.preventExtensions(obj1);
obj1 === obj2;
console.log(obj1 + " " + "Objeto no extendible" );
//----------------------------------------------------------
//Error al extender
var nonExtensible = { removable: true };
Object.preventExtensions(nonExtensible);
Object.defineProperty(nonExtensible, 'new', { value: 8675309 });
console.log(nonExtensible);